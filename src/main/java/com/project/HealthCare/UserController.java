package com.project.HealthCare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.model.Doctor;
import com.model.User;



@RestController
public class UserController {
	
	@Autowired
	UserDAO userDAO;
	
	@RequestMapping("getAllUsers")
	public List<User> getAllUsers() {
		return userDAO.getAllUsers();
	}
	
	@RequestMapping("getUserById/{id}")
	public User getUserById(@PathVariable("id") int id) {
		return userDAO.getUserById(id);
	}
	
	@RequestMapping("getUserByName/{name}")
	public User getUserByName(@PathVariable("name") String name) {
		return userDAO.getUserByName(name);
	}
	
	@PostMapping("registerUser")
	public User registerUser(@RequestBody User user) {
		return userDAO.registerUser(user);
	}
	
	@PutMapping("updateUser")
	public User updateUser(@RequestBody User user) {
		return userDAO.updateUser(user);
	}
	
	@DeleteMapping("deleteUser/{id}")
	public String deleteUser(@PathVariable int id) {
		userDAO.deleteUser(id);
		return "User Deleted!!";
	}
	

	@GetMapping("userlogin/{emailId}/{password}")
	public User login(@PathVariable("emailId") String emailId,@PathVariable("password") String password){
		return userDAO.login(emailId, password);
	}
	
	@RequestMapping("userList/{docId}")
	public List<User> userList(@PathVariable("docId") int docId){
		return userDAO.userList(docId);
	}
	@RequestMapping("checkUserEmail/{emailId}")
	public User checkEmail(@PathVariable("emailId") String emailId) {
		return userDAO.checkEmail(emailId);
	}     

}
