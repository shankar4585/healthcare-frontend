package com.project.HealthCare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DoctorDAO;
import com.model.Doctor;



@RestController
public class DoctorController {
	
	@Autowired
	DoctorDAO docDAO;
	
	@RequestMapping("getAllDoctors")
	public List<Doctor> getAllDoctors() {
		return docDAO.getAllDoctors();
	}
	
	@RequestMapping("getDoctorById/{id}")
	public Doctor getDoctorById(@PathVariable("id") int id) {
		return docDAO.getDoctorById(id);
	}
	
	@RequestMapping("getDoctorByName/{name}")
	public Doctor getDepartmentByName(@PathVariable("name") String name)         
        {
		return docDAO.getDoctorByName(name);
	}
	
	@PostMapping("registerDoctor")
	public Doctor registerDepartment(@RequestBody Doctor doc) {
		return docDAO.registerDoctor(doc);
	}
	
	@PutMapping("updateDoctor")
	public Doctor updateDepartment(@RequestBody Doctor doc) {
		return docDAO.updateDoctor(doc);
	}
	
	@DeleteMapping("deleteDoctor/{id}")
	public String deleteDoctor(@PathVariable int id) {
		docDAO.deleteDoctor(id);
		return "Doctor Deleted!!";
	}
	@GetMapping("doclogin/{loginId}/{password}")
	public Doctor login(@PathVariable("loginId") String loginId,@PathVariable("password") String password){
		return docDAO.login(loginId, password);
	}
	@RequestMapping("checkDoctorEmail/{loginId}")
	public Doctor checkEmail(@PathVariable("loginId") String loginId) {
		return docDAO.checkEmail(loginId);
	}

}
