package com.project.HealthCare;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dao.ProductsDAO;
import com.model.Products;



@RestController
public class ProductController {
	
	@Autowired
	ProductsDAO prodDAO;
	
	@RequestMapping("getAllProducts")
	public List<Products> getAllProducts() {
		return prodDAO.getAllProducts();
	}
	
	@PostMapping("registerProduct")
	public Products registerProduct(@RequestBody Products product) {
		return prodDAO.registerProduct(product);
	}
	
	@PutMapping("updateProduct")
	public Products updateProduct(@RequestBody Products product) {
		return prodDAO.updateProduct(product);
	}
	
	@DeleteMapping("deleteProduct/{id}")
	public String deleteProduct(@PathVariable int id) {
		prodDAO.deleteProduct(id);
		return "Product Deleted!!";
	}

}
