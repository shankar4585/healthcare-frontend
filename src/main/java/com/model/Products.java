package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Products {
	
	@Id @GeneratedValue
	private int id;
	@Column(name="productName")
	private String name;
	private double price;
	private String discription;
	private String img;
	
	public Products() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Products(int id, String name, double price, String discription, String img) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.discription = discription;
		this.img = img;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "Products [id=" + id + ", name=" + name + ", price=" + price + ", discription=" + discription + ", img="
				+ img + "]";
	}
	
	
	
	
	
	

}
