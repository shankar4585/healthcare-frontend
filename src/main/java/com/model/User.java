package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class User {
	@Id @GeneratedValue
	private int userid;
	@Column(name="userName")
	private String name;
	
	private String emailId;
	private String password;
	private String dob;
	private String moblieNumber;
	private String gender;
	private String address;
	private int age;
	private String bloodGroup;
	private String concern;
	
	@ManyToOne
	@JoinColumn(name="docId")
	private Doctor doctor;
	
	private String description;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int userid, String name, String emailId, String password, String dob, String moblieNumber,
			String gender, String address, int age, String bloodGroup, String concern, Doctor doctor,
			String description) {
		super();
		this.userid = userid;
		this.name = name;
		this.emailId = emailId;
		this.password = password;
		this.dob = dob;
		this.moblieNumber = moblieNumber;
		this.gender = gender;
		this.address = address;
		this.age = age;
		this.bloodGroup = bloodGroup;
		this.concern = concern;
		this.doctor = doctor;
		this.description = description;
	}

	@Override
	public String toString() {
		return "User [userid=" + userid + ", name=" + name + ", emailId=" + emailId + ", password=" + password
				+ ", dob=" + dob + ", moblieNumber=" + moblieNumber + ", gender=" + gender + ", address=" + address
				+ ", age=" + age + ", bloodGroup=" + bloodGroup + ", concern=" + concern + ", doctor=" + doctor
				+ ", description=" + description + "]";
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getMoblieNumber() {
		return moblieNumber;
	}

	public void setMoblieNumber(String moblieNumber) {
		this.moblieNumber = moblieNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBloodGroup() {
		return bloodGroup;
	}

	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}

	public String getConcern() {
		return concern;
	}

	public void setConcern(String concern) {
		this.concern = concern;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
	
	

}
