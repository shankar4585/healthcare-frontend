package com.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Doctor {
	
	@Id @GeneratedValue
	private int id;
	@Column(name="docName")
	private String name;
	private String loginId;
	private String password;
	private String specilzation;
	private String designation;
	
	@JsonIgnore
	@OneToMany(mappedBy="doctor")
	List<User> userList = new ArrayList<User>();
	
	
	public Doctor() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Doctor(int id, String name, String loginId, String password, String specilzation, String designation,
			List<User> userList) {
		super();
		this.id = id;
		this.name = name;
		this.loginId = loginId;
		this.password = password;
		this.specilzation = specilzation;
		this.designation = designation;
		this.userList = userList;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLoginId() {
		return loginId;
	}


	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getSpecilzation() {
		return specilzation;
	}


	public void setSpecilzation(String specilzation) {
		this.specilzation = specilzation;
	}


	public String getDesignation() {
		return designation;
	}


	public void setDesignation(String designation) {
		this.designation = designation;
	}


	public List<User> getUserList() {
		return userList;
	}


	public void setUserList(List<User> userList) {
		this.userList = userList;
	}


	@Override
	public String toString() {
		return "Doctor [id=" + id + ", name=" + name + ", loginId=" + loginId + ", password=" + password
				+ ", specilzation=" + specilzation + ", designation=" + designation + ", userList=" + userList + "]";
	}	
	
	

}
