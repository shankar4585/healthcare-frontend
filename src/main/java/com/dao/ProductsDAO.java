package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Products;



@Service
public class ProductsDAO {
	
	@Autowired
	ProductsRepository prodRepo;
	
	public List<Products> getAllProducts() {
		return prodRepo.findAll();
	}
	
	public Products registerProduct(Products product) {
		return prodRepo.save(product);
	}
	
	public Products updateProduct(Products product) {
		return prodRepo.save(product);
	}
	
	public void deleteProduct(int id) {
		prodRepo.deleteById(id);
	}

}
