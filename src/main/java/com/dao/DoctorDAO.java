package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.model.Doctor;

@Service
public class DoctorDAO {
	
	@Autowired
	DoctorRepository docRepo;
	public List<Doctor> getAllDoctors() {
		return docRepo.findAll();
	}
	
	public Doctor getDoctorById(int id) {
		Doctor doc = new Doctor();
		return docRepo.findById(id).orElse(doc);
	}
	
	public Doctor getDoctorByName(String name) {
		return docRepo.findByName(name);
	}
	public Doctor registerDoctor(Doctor doc) {
		return docRepo.save(doc);
	}
	public Doctor updateDoctor(Doctor doc) {
		return docRepo.save(doc);
	}
	public void deleteDoctor(int id) {
		docRepo.deleteById(id);
	}
	public Doctor login(String loginId,String password){
		return docRepo.login(loginId, password);
	}
	public Doctor checkEmail(String loginId){
		return docRepo.findByEmail(loginId);
	}
	

}
