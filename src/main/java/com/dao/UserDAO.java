package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Doctor;
import com.model.User;

@Service
public class UserDAO {
	
	@Autowired
	UserRepository userRepo;
	
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	public User getUserById(int id) {
		User user = new User();
		return userRepo.findById(id).orElse(user);
	}

	public User getUserByName(String name) {
		return userRepo.findByName(name) ;
	}

	public User registerUser(User user) {
		return userRepo.save(user);
	}
	
	public User updateUser(User user) {
		return userRepo.save(user);
	}
	
	public void deleteUser(int id) {
		userRepo.deleteById(id);
	}
	public User login(String emailId, String password) {
	    return userRepo.login(emailId, password);
	}
	public List<User> userList(int docId) {
	    return userRepo.userList(docId);
	}
	public User checkEmail(String emailId){
		return userRepo.findByEmail(emailId);
	}

}
