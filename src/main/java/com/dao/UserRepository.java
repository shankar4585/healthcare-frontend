package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Doctor;
import com.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	User findByName(String name);
	@Query("from User u where u.emailId=:emailId and u.password=:password")
	public User login(@Param("emailId") String emailId,@Param("password") String password);
	@Query("from User u where u.doctor.id=:docId")
	public List<User> userList(@Param("docId") int docId);
	
	@Query("from User u where u.emailId=:emailId")
	public User findByEmail(@Param("emailId") String emailId);
	
	

}
