package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.model.Doctor;
@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

	Doctor findByName(String name);
	@Query("from Doctor d where d.loginId=:loginId and d.password=:password")
	public Doctor login(@Param("loginId") String loginId,@Param("password") String password);

	@Query("from Doctor d where d.loginId=:loginId")
	public Doctor findByEmail(@Param("loginId") String loginId);

}
